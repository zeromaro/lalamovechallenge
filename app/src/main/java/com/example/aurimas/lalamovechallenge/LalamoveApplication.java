package com.example.aurimas.lalamovechallenge;

import android.app.Application;
import android.util.Log;

import com.example.aurimas.lalamovechallenge.helper.Constants;
import com.google.gson.GsonBuilder;
import com.orm.SugarContext;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Aurimas on 26/05/2017.
 */


public class LalamoveApplication extends Application {

    private Retrofit restAdapter;

    public Retrofit getRestAdapter() {
        return restAdapter;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setUpRetrofit();
        initSugarOrm();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }

    private void initSugarOrm() {
        SugarContext.init(this);
    }

    private void setUpRetrofit() {
        File httpCacheDir = null;
        Cache cache = null;
        try {
            httpCacheDir = new File(getApplicationContext().getCacheDir(), "httpResponses");
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            cache = new Cache(httpCacheDir, httpCacheSize);

        } catch (Exception e) {
            Log.e("Retrofit", "Could not create http cache", e);
        }
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.interceptors().add(interceptor);
        httpClient.connectTimeout(Constants.TIMEOUT, TimeUnit.SECONDS);
        httpClient.readTimeout(Constants.TIMEOUT, TimeUnit.SECONDS);
        httpClient.writeTimeout(Constants.TIMEOUT, TimeUnit.SECONDS);
        httpClient.addInterceptor(getLoggingInterceptor());
        httpClient.cache(cache);

        OkHttpClient okHttpClient = httpClient.build();

        restAdapter = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();
    }

    private HttpLoggingInterceptor getLoggingInterceptor()
    {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return loggingInterceptor;
    }
}
