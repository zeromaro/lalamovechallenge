package com.example.aurimas.lalamovechallenge.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aurimas.lalamovechallenge.R;
import com.example.aurimas.lalamovechallenge.model.DeliveryItem;

import java.util.List;

/**
 * Created by Aurimas on 26/05/2017.
 */

public class DeliveriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface OnDeliveryItemClickListener {
        void onDeliveryItemClick(DeliveryItem deliveryItem);
    }

    private static class DeliveryItemViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout rootLayout;
        private ImageView itemIv;
        private TextView itemDescriptionTv;

        public DeliveryItemViewHolder(View itemView) {
            super(itemView);
            rootLayout = (RelativeLayout) itemView.findViewById(R.id.deliveryItemRootLayout);
            itemIv = (ImageView) itemView.findViewById(R.id.deliveryItemIv);
            itemDescriptionTv = (TextView) itemView.findViewById(R.id.deliveryItemDescriptionTv);
        }
    }

    private Context context;
    private List<DeliveryItem> deliveryItemList;
    private OnDeliveryItemClickListener onDeliveryItemClickListener;

    public DeliveriesAdapter(Context context, List<DeliveryItem> deliveryItemList) {
        this.context = context;
        this.deliveryItemList = deliveryItemList;
    }

    //what you want to do is just update the list and call notifyrangechanged/notifyItemInserted/Removed -> provides animations
    //lazy solution to demonstrate understanding of reset
    public void resetList() {
        deliveryItemList.clear();
        notifyDataSetChanged();
    }

    public void setOnDeliveryItemClickListener(Context context) {
        onDeliveryItemClickListener = (OnDeliveryItemClickListener) context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DeliveryItemViewHolder(LayoutInflater.from(context).inflate(R.layout.delivery_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final DeliveryItem deliveryItem = deliveryItemList.get(position);
        Glide.with(context).load(deliveryItem.getImageUrl())
                .centerCrop()
                .into(((DeliveryItemViewHolder) holder).itemIv);
        ((DeliveryItemViewHolder) holder).itemDescriptionTv.setText(deliveryItem.getDescription());
        ((DeliveryItemViewHolder) holder).rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeliveryItemClickListener.onDeliveryItemClick(deliveryItem);
            }
        });

    }

    @Override
    public int getItemCount() {
        return deliveryItemList.size();
    }
}
