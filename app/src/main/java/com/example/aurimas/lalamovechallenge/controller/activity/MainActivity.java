package com.example.aurimas.lalamovechallenge.controller.activity;

import android.content.pm.ActivityInfo;
import android.support.annotation.IntDef;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.aurimas.lalamovechallenge.LalamoveApplication;
import com.example.aurimas.lalamovechallenge.R;
import com.example.aurimas.lalamovechallenge.adapter.DeliveriesAdapter;
import com.example.aurimas.lalamovechallenge.controller.fragment.DeliveriesFragment;
import com.example.aurimas.lalamovechallenge.controller.fragment.DeliveryFragment;
import com.example.aurimas.lalamovechallenge.helper.DbHelper;
import com.example.aurimas.lalamovechallenge.helper.RandomHelper;
import com.example.aurimas.lalamovechallenge.interfaces.MainActivityTitleChangeListener;
import com.example.aurimas.lalamovechallenge.model.DeliveryItem;

public class MainActivity extends FragmentActivity implements DeliveriesAdapter.OnDeliveryItemClickListener,
        MainActivityTitleChangeListener, DeliveriesFragment.OnResetClickListener {
    private static final String TAG = "MainActivity";

    public static final int REPLACE_FRAGMENT = 1;
    public static final int ADD_FRAGMENT = 2;

    @IntDef({REPLACE_FRAGMENT, ADD_FRAGMENT})
    private @interface FragmentAction{};

    private FrameLayout frameLayout, frameLayoutPaneLeft, frameLayoutPaneRight;
    private TextView titleTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titleTv = (TextView) findViewById(R.id.mainTitleTv);

        checkIfDeviceIsTabletAndLockOrientation();

        if (!isMultiPaneMode()) {
            frameLayout = (FrameLayout) findViewById(R.id.mainActivityFrameLayout);
            showFragment(frameLayout, DeliveriesFragment.newInstance("z41z1"), ADD_FRAGMENT, false);
        } else {
            frameLayoutPaneLeft = (FrameLayout) findViewById(R.id.mainActivityDeliveriesFragment);
            frameLayoutPaneRight = (FrameLayout) findViewById(R.id.mainActivityDeliveryFragment);
            showFragment(frameLayoutPaneLeft, frameLayoutPaneRight);
        }
    }

    private void checkIfDeviceIsTabletAndLockOrientation() {
        if (RandomHelper.isTablet(this))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        else
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
    }

    private void showFragment(FrameLayout frameLayoutPaneLeft, FrameLayout frameLayoutPaneRight) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(frameLayoutPaneLeft.getId(), DeliveriesFragment.newInstance("z41z1"));
        fragmentTransaction.add(frameLayoutPaneRight.getId(), DeliveryFragment.newInstance(null));
        fragmentTransaction.commit();
    }

    private void showFragment(FrameLayout frameLayout, Fragment fragment, @FragmentAction int fragmentAction, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (!RandomHelper.isTablet(this))
            fragmentTransaction.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit);

        switch (fragmentAction) {
            case REPLACE_FRAGMENT:
                fragmentTransaction.replace(frameLayout.getId(), fragment);
                break;
            case ADD_FRAGMENT:
                fragmentTransaction.add(frameLayout.getId(), fragment);
                break;
        }

        if (addToBackStack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private boolean isMultiPaneMode() {
        return findViewById(R.id.mainActivityFrameLayout) == null;
    }


    @Override
    public void onDeliveryItemClick(DeliveryItem deliveryItem) {
        if (RandomHelper.isTablet(this))
            showFragment(frameLayoutPaneRight, DeliveryFragment.newInstance(deliveryItem), ADD_FRAGMENT, true);
        else
            showFragment(frameLayout, DeliveryFragment.newInstance(deliveryItem), REPLACE_FRAGMENT, true);
    }

    @Override
    public void onEnterFragment(String text) {
        if (RandomHelper.isTablet(this))
            titleTv.setText(getString(R.string.deliveries_fragment_title) + " and " + getString(R.string.delivery_fragment_title));
        else
            titleTv.setText(text);
    }

    @Override
    public void onResetClicked() {
        DbHelper.deleteAllItems();


        if (RandomHelper.isTablet(this)) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            showFragment(frameLayoutPaneRight, DeliveryFragment.newInstance(null), REPLACE_FRAGMENT, false);
        }

    }
}
