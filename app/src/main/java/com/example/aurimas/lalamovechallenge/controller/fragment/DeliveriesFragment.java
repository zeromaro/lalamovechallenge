package com.example.aurimas.lalamovechallenge.controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.aurimas.lalamovechallenge.LalamoveApplication;
import com.example.aurimas.lalamovechallenge.R;
import com.example.aurimas.lalamovechallenge.adapter.DeliveriesAdapter;
import com.example.aurimas.lalamovechallenge.controller.activity.MainActivity;
import com.example.aurimas.lalamovechallenge.helper.Constants;
import com.example.aurimas.lalamovechallenge.helper.DbHelper;
import com.example.aurimas.lalamovechallenge.interfaces.MainActivityTitleChangeListener;
import com.example.aurimas.lalamovechallenge.model.DeliveryItem;
import com.example.aurimas.lalamovechallenge.request.DeliveryService;
import com.example.aurimas.lalamovechallenge.helper.LalamoveRetrofitCallback;
import com.example.aurimas.lalamovechallenge.helper.LalamoveRetrofitCallbackInterface;

import java.util.List;

import retrofit2.Call;

/**
 * Created by Aurimas on 26/05/2017.
 */

public class DeliveriesFragment extends Fragment {
    private static final String TAG = "DeliveriesFragment";

    public interface OnResetClickListener
    {
        void onResetClicked();
    }

    private RecyclerView recyclerView;
    private String path;
    private ProgressBar progressBar;
    private Call<List<DeliveryItem>> call;
    private DeliveriesAdapter deliveriesAdapter;
    private MainActivityTitleChangeListener mainActivityTitleChangeListener;
    private OnResetClickListener onResetClickListener;
    private Button refreshBtn;

    public static DeliveriesFragment newInstance(String path) {

        Bundle args = new Bundle();
        args.putString(Constants.PATH_KEY, path);
        DeliveriesFragment fragment = new DeliveriesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onResetClickListener = (OnResetClickListener)context;
        mainActivityTitleChangeListener = (MainActivityTitleChangeListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        path = getArguments().getString(Constants.PATH_KEY);
        initCall(path);
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivityTitleChangeListener.onEnterFragment(getString(R.string.deliveries_fragment_title));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onResetClickListener = null;
        mainActivityTitleChangeListener = null;
    }

    private void initCall(String path) {
        DeliveryService deliveryService = ((LalamoveApplication)getActivity().getApplicationContext()).getRestAdapter().create(DeliveryService.class);
        call = deliveryService.getItems(path);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.deliveries_fragment, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.mainActivityProgressBar);
        recyclerView = (RecyclerView) view.findViewById(R.id.deliveriesFragmentRecyclerView);
        refreshBtn = (Button)view.findViewById(R.id.deliveriesFragmentRefreshBtn);
        refreshBtn.setOnClickListener(onRefreshClickListener);

        if(DbHelper.isLocalDbEmpty())
            getItems();
        else
            setUpRecyclerView(DbHelper.getDeliveryItems());

        return view;
    }

    private void getItems() {
        call.clone().enqueue(new LalamoveRetrofitCallback<>(getContext(), progressBar, new LalamoveRetrofitCallbackInterface<List<DeliveryItem>>() {
                    @Override
                    public void onSuccess(final List<DeliveryItem> response) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                DbHelper.saveAllItems(response);
                            }
                        });
                        setUpRecyclerView(response);
                    }

                    @Override
                    public void onFailure(List<DeliveryItem> response) {
                    }

                    @Override
                    public void onNetworkError(Throwable throwable) {
                    }
                }));
    }

    private void setUpRecyclerView(List<DeliveryItem> deliveryItemList) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        deliveriesAdapter = new DeliveriesAdapter(getContext(), deliveryItemList);
        deliveriesAdapter.setOnDeliveryItemClickListener(getContext());
        recyclerView.setAdapter(deliveriesAdapter);
    }

    private View.OnClickListener onRefreshClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onResetClickListener.onResetClicked();
            deliveriesAdapter.resetList();
            getItems();
        }
    };
}
