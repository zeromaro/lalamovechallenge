package com.example.aurimas.lalamovechallenge.controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aurimas.lalamovechallenge.R;
import com.example.aurimas.lalamovechallenge.helper.Constants;
import com.example.aurimas.lalamovechallenge.helper.RandomHelper;
import com.example.aurimas.lalamovechallenge.interfaces.MainActivityTitleChangeListener;
import com.example.aurimas.lalamovechallenge.model.DeliveryItem;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Aurimas on 26/05/2017.
 */

public class DeliveryFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = "DeliveryFragment";

    private GoogleMap map;
    private ImageView itemIv;
    private TextView itemDescriptionTv;
    private DeliveryItem deliveryItem;
    private MainActivityTitleChangeListener mainActivityTitleChangeListener;

    public static DeliveryFragment newInstance(DeliveryItem deliveryItem) {

        Bundle args = new Bundle();
        args.putParcelable(Constants.DELIVERY_ITEM_KEY, deliveryItem);
        DeliveryFragment fragment = new DeliveryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivityTitleChangeListener = (MainActivityTitleChangeListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
            deliveryItem = getArguments().getParcelable(Constants.DELIVERY_ITEM_KEY);
        else
            deliveryItem = savedInstanceState.getParcelable(Constants.DELIVERY_ITEM_KEY);
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivityTitleChangeListener.onEnterFragment(getString(R.string.delivery_fragment_title));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainActivityTitleChangeListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.delivery_fragment, container, false);

        View itemView = view.findViewById(R.id.deliveryFragmentItemView);
        itemIv = (ImageView) itemView.findViewById(R.id.deliveryItemIv);
        itemDescriptionTv = (TextView) itemView.findViewById(R.id.deliveryItemDescriptionTv);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.deliveryFragmentMap);
        mapFragment.getMapAsync(this);

        hideItemViewIfTablet(itemView);


        return view;
    }

    private void hideItemViewIfTablet(View itemView) {
        if (deliveryItem != null) {
            itemView.setVisibility(View.VISIBLE);
            setUpClickedItem();
        } else
            itemView.setVisibility(View.INVISIBLE);
    }

    private void setUpClickedItem() {
        Glide.with(getContext()).load(deliveryItem.getImageUrl())
                .centerCrop()
                .into(itemIv);
        itemDescriptionTv.setText(deliveryItem.getDescription());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Constants.DELIVERY_ITEM_KEY, deliveryItem);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (deliveryItem != null) {
            LatLng location = new LatLng(deliveryItem.getLat(), deliveryItem.getLng());

            map = googleMap;
            map.addMarker(new MarkerOptions().position(location).title(deliveryItem.getDescription()));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(location, 15);
            map.animateCamera(cameraUpdate);
        }
    }
}
