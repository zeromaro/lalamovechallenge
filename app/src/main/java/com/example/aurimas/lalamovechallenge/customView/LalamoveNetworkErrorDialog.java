package com.example.aurimas.lalamovechallenge.customView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ProgressBar;

import com.example.aurimas.lalamovechallenge.R;
import com.example.aurimas.lalamovechallenge.helper.RandomHelper;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Aurimas on 27/05/2017.
 */

public class LalamoveNetworkErrorDialog<T> extends AlertDialog.Builder {
    public LalamoveNetworkErrorDialog(Context context, final ProgressBar progressBar, final Call<T> call, final Callback<T> callback) {
        super(context);
        setTitle(R.string.network_error_dialog_title);
        setMessage(R.string.network_error_dialog_message);
        setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RandomHelper.showProgressBar(progressBar);
                call.clone().enqueue(callback);
            }
        });
        show();
    }


}
