package com.example.aurimas.lalamovechallenge.helper;

/**
 * Created by Aurimas on 25/05/2017.
 */

public class Constants {
    public static final String BASE_URL = "https://api.myjson.com";
    public static final String PATH_KEY = "path";
    public static final String DELIVERY_ITEM_KEY = "deliveryItem";
    public static final int TIMEOUT = 15;
}

