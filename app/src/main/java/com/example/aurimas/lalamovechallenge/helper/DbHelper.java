package com.example.aurimas.lalamovechallenge.helper;

import com.example.aurimas.lalamovechallenge.model.DeliveryItem;
import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by Aurimas on 26/05/2017.
 */

public class DbHelper {

    public static boolean isLocalDbEmpty()
    {
        return DeliveryItem.listAll(DeliveryItem.class).size() == 0;
    }

    public static List<DeliveryItem> getDeliveryItems()
    {
        return DeliveryItem.listAll(DeliveryItem.class);
    }

    public static void saveAllItems(List<DeliveryItem> deliveryItems)
    {
        //bulk insert
//        SugarRecord.saveInTx(deliveryItems);
        for(int i=0; i<deliveryItems.size(); i++) {
            deliveryItems.get(i).modifyOutterLocationValues();
            DeliveryItem.save(deliveryItems.get(i));
        }
    }

    public static void deleteAllItems()
    {
        SugarRecord.deleteAll(DeliveryItem.class);
    }
}
