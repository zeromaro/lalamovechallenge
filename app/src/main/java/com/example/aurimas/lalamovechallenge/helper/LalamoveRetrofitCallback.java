package com.example.aurimas.lalamovechallenge.helper;

import android.content.Context;
import android.widget.ProgressBar;

import com.example.aurimas.lalamovechallenge.customView.LalamoveNetworkErrorDialog;

import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aurimas on 27/05/2017.
 */

public class LalamoveRetrofitCallback<T> implements Callback<T> {

    private Context context;
    private ProgressBar progressBar;
    private LalamoveRetrofitCallbackInterface<T> lalamoveRetrofitCallbackInterface;

    public LalamoveRetrofitCallback(Context context, ProgressBar progressBar, LalamoveRetrofitCallbackInterface<T> lalamoveRetrofitCallbackInterface) {
        this.context = context;
        this.progressBar = progressBar;
        this.lalamoveRetrofitCallbackInterface = lalamoveRetrofitCallbackInterface;

        RandomHelper.showProgressBar(progressBar);
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        RandomHelper.hideProgressBar(progressBar);

        if (response.code() >= 400 && response.code() < 599)
            lalamoveRetrofitCallbackInterface.onFailure(response.body());
        else
            lalamoveRetrofitCallbackInterface.onSuccess(response.body());

    }

    @Override
    public void onFailure(final Call<T> call, Throwable t) {
        RandomHelper.hideProgressBar(progressBar);
        if (t instanceof UnknownHostException) {
            lalamoveRetrofitCallbackInterface.onNetworkError(t);
            new LalamoveNetworkErrorDialog<T>(context, progressBar, call, this);
        }
    }
}
