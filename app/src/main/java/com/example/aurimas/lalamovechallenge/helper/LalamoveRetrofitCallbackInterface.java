package com.example.aurimas.lalamovechallenge.helper;

import com.example.aurimas.lalamovechallenge.model.DeliveryItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aurimas on 27/05/2017.
 */

public interface LalamoveRetrofitCallbackInterface<T> {
    void onSuccess(T response);

    void onFailure(T response);

    void onNetworkError(Throwable throwable);
}
