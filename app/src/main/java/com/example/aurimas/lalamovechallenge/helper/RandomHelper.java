package com.example.aurimas.lalamovechallenge.helper;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.aurimas.lalamovechallenge.R;
import com.example.aurimas.lalamovechallenge.model.DeliveryItem;
import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by Aurimas on 25/05/2017.
 */

public class RandomHelper {

    public static int getScreenHeight(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.heightPixels;
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    public static void hideProgressBar(ProgressBar progressBar) {
        if (progressBar.getVisibility() == View.VISIBLE)
            progressBar.setVisibility(View.INVISIBLE);
    }

    public static void showProgressBar(ProgressBar progressBar)
    {
        if(progressBar.getVisibility() == View.INVISIBLE)
            progressBar.setVisibility(View.VISIBLE);
    }

    public static boolean isTablet(Context context)
    {
        return context.getResources().getBoolean(R.bool.is_tablet);
    }
}
