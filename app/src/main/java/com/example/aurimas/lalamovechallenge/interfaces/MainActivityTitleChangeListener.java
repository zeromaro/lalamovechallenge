package com.example.aurimas.lalamovechallenge.interfaces;

/**
 * Created by Aurimas on 26/05/2017.
 */

public interface MainActivityTitleChangeListener {
    void onEnterFragment(String text);
}
