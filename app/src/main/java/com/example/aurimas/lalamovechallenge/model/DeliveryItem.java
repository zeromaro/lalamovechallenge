package com.example.aurimas.lalamovechallenge.model;

/**
 * Created by Aurimas on 26/05/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class DeliveryItem extends SugarRecord implements Parcelable{
    private static final String TAG = "DeliveryItem";

    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("location")
    @Expose
    private DeliveryItemLocation location;

    public DeliveryItem()
    {
    }

    protected DeliveryItem(Parcel in) {
        description = in.readString();
        imageUrl = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        address = in.readString();
    }

    public static final Creator<DeliveryItem> CREATOR = new Creator<DeliveryItem>() {
        @Override
        public DeliveryItem createFromParcel(Parcel in) {
            return new DeliveryItem(in);
        }

        @Override
        public DeliveryItem[] newArray(int size) {
            return new DeliveryItem[size];
        }
    };

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private DeliveryItemLocation getLocation() {
        return location;
    }


    //I couldn't figure out why my location object is not being saved into database therefore I am
    //extracting location fields outside the Location object
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("address")
    @Expose
    private String address;
    
    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }
    
    public void modifyOutterLocationValues()
    {
        this.lat = location.getLat();
        this.lng = location.getLng();
        this.address = location.getAddress();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(description);
        dest.writeString(imageUrl);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeString(address);
    }

    private class DeliveryItemLocation extends SugarRecord{
        @SerializedName("lat")
        @Expose
        private Double lat;
        @SerializedName("lng")
        @Expose
        private Double lng;
        @SerializedName("address")
        @Expose
        private String address;

        public DeliveryItemLocation() {
        }

        private Double getLat() {
            return lat;
        }

        private void setLat(Double lat) {
            this.lat = lat;
        }

        private Double getLng() {
            return lng;
        }

        private void setLng(Double lng) {
            this.lng = lng;
        }

        private String getAddress() {
            return address;
        }

        private void setAddress(String address) {
            this.address = address;
        }

    }
}
