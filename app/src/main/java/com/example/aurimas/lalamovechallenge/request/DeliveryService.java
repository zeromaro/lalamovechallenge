package com.example.aurimas.lalamovechallenge.request;

import com.example.aurimas.lalamovechallenge.model.DeliveryItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Aurimas on 26/05/2017.
 */

public interface DeliveryService {
//    instead of /bins/{path} should be /deliveries but I don't have NODEjs therefore uploaded JSON to external server

    @GET("/bins/{path}")
    Call<List<DeliveryItem>> getItems(@Path("path") String pathId);
}
